package cn.pdy.try_method.jdk.java.util;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * scanner主要提供了两个方法来扫描输入：
 * hasNextXxx()   是否含有下一个输入项,true,false
 * nextXxx()      获取下一个输入项
 * 注：Xxx可以是 int, long ,double 等代表基本数据类型的字符串
 */
public class ScannerTest {

    public static void main(String[] args) {}

    /**
     * 读取键盘输入
     */
    @Test
    public void scannerKeyBoardTest(){
        Scanner sc= new Scanner(System.in);

        //useDelimiter(String pattern) 设置scanner的分隔符为换行符,逐行读取,默认为空格
        //也可使用 hasNextLine, nextLine  来逐行读取
        sc.useDelimiter("\n");

        while (sc.hasNext()){
            String s = sc.next();
            if (s.equals("quit")){
                break;
            }
            System.out.println(s);
        }
        System.out.println("over");
    }

    /**
     * 可以读取文件输入，传入file对象作为参数
     */
    @Test
    public void scannerFileTest() throws FileNotFoundException {
        Scanner sc= new Scanner(new File("src/main/resources/ioTest.txt"));

        while(sc.hasNextLine()){
            System.out.println(sc.nextLine());
        }
    }

}
