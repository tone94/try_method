package cn.pdy.try_method.jdk.java.math;

import java.math.BigDecimal;

/**
 * float,double容易引起精度丢失
 *
 * 该类提供了大量的构造器用于构造BigDecimal对象, 推荐使用String的构造器, 建议不要使用 double的构造器
 * 如果一定要用double, 要用BigDecimal.valueOf(double n) 静态方法来创建BigDecimal对象
 *
 * 提供了 add加, subtract减, multiply乘, divide除, pow 等方法对精确浮点数进行常规算术运算
 *
 * 如果程序中要求对double浮点数进行基本运算, 则需要先将double包装成BigDecimal对象, 调用BD对象的方法进行运算后再将结果转换成double类型变量.
 * 这个过程比较繁琐, 可以考虑建一个工具类
 */
public class BigDecimalTest {

    public static void main(String[] args) {

        BigDecimal bd = new BigDecimal("0.1");
        BigDecimal bd2 = bd.add(new BigDecimal("0.2"));
        System.out.println(bd2.doubleValue());

        //精度丢失
        double d1 = 0.1;
        double d2 = 0.2;
        System.out.println(d1+d2);

    }
}
