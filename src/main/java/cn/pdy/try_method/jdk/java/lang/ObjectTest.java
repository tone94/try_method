package cn.pdy.try_method.jdk.java.lang;

/**
 * 所有java类都是Object的子类
 */
public class ObjectTest {

    public static void main(String[] args) {
        Object obj1 = new Object();
        Object obj2 = new Object();
        // 常用方法
        // 1.equals
        boolean flag = obj1.equals(obj2);
        // 2.protect void finalize() 垃圾回收器调用此方法来清理该对象的资源
        // 3.getClass() 返回该对象的运行时类
        Class<?> aClass = obj1.getClass();
        // 4.int hashCode() 返回该对象的hashCode值,很多类都重写了该方法
        // 5.String toString() 返回该对象的字符串表示
        // 其他: wait,notify,notifyAll 控制线程的暂停和运行
        //      clone 实现自我克隆
    }

}
