package cn.pdy.try_method.jdk.java.atemp;


import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {
    /*
    正则表达式 : 用于匹配字符串的模板
    String 类里提供了几个正则的方法:
        matches(String regex) -- 判断该字符是否匹配指定的正则表达式
        replaceAll(String regex,String replacement) -- 将该字符串中所有匹配regex的子串替换成replacement
        replaceFirst -- 同上, 匹配第一个
        String[] split(String regex) -- 以regex作为分隔符,把该字符串分割成多个子串
     此外, Java还提供了 Pattern 和 Matcher 两个类专门用于提供正则表达式支持.
    */

    /*
      正则中的预定义字符:  d 数字, w 单词(0-9,数字,字母,下划线), s 空白字符
                          D, W, S 的含义与之相反,匹配非数字,非单词,非空白字符
      方括号表达式:
                    [] ,表示枚举
                    [x-x] , - 表示范围
                    [^xxx] ,[^x-x] , ^ 表示求否
                    && 与, 求交集
                    [xx[x-x]] 并
    */

    @Test
    public void patternTest(){
        // Pattern 对象是正则表达式编译后再内存中的表示形式, 因此, 正则表达式字符串必须先被编译为 Pattern 对象,
        // 然后再利用该 Pattern 对象创建对应的 Matcher 对象.执行匹配所涉及的状态保留在 Mathcer 对象中.
        // 一旦在程序中定义了正则表达式,就可以使用 Pattern 和 Matcher 来使用正则表达式
        // Pattern 是不可变类 , 可供多个并发线程安全使用

        Pattern pattern = Pattern.compile("a*b");
        Matcher matcher = pattern.matcher("aaaaaaab");
        boolean matches = matcher.matches();
        System.out.println(matches);

        // 如果正则表达式只试用一次,则可使用Pattern类的静态 matches() 方法
        // 此方法自动把指定字符串编译成匿名的Pattern对象, 并执行匹配
        boolean matches1 = Pattern.matches("a*b", "aaaab");
        System.out.println(matches1);

        // Matcher 类常用方法
        //boolean b = matcher.find(); // 返回目标字符串中是否包含于Pattern匹配的字符串
        //String group = matcher.group(); // 返回上一次与Pattern匹配的字符串
        //int start = matcher.start(); // 返回上一次与pattern匹配的字符串在目标字符串中的开始位置
        //int end = matcher.end(); // 结束位置+1
        //boolean b1 = matcher.lookingAt(); // 返回目标字符串前面部分与pattern是否匹配
        //boolean matches2 = matcher.matches(); // 返回整个目标字符串与pattern是否匹配
        //Matcher reset = matcher.reset(); // 将现有的Matcher对象应用于一个新的字符序列

        Matcher matcher1 = pattern.matcher("ahjaabsafb");
        boolean b2 = matcher1.find();
        System.out.println(b2);
    }

    /**
     * 从大段字符串中找电话号码
     */
    @Test
    public void findNumber(){
        String str = "hhfkdshf 13517584676."+"njfsghdf 15646736587 mklajfs"+"地方°锁定水电费 15867564763";
        //创建正则对象
        Pattern pattern = Pattern.compile("((13\\d)|(15\\d))\\d{8}");
        Matcher m = pattern.matcher(str);
        while (m.find()){
            System.out.println(m.group());
        }
    }



}
