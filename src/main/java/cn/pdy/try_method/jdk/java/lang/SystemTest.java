package cn.pdy.try_method.jdk.java.lang;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * System 类代表当前java程序的运行平台,程序不能创建System类的对象
 * System 提供了 gc(), runFinalization() 方法
 */
public class SystemTest {

    public static void main(String[] args) { }

    /**
     * 访问操作的环境变量和系统属性
     */
    @Test
    public void m1() throws IOException {
        //获取系统所有的环境变量
        Map<String, String> env = System.getenv();
        for (String name:env.keySet()) {
            System.out.println(env.get(name));
        }

        //获取指定环境变量的值
        System.out.println(System.getenv("JAVA_HOME"));
        //获取所有的系统属性
        Properties prop = System.getProperties();
        prop.store(new FileOutputStream("props.txt"),"system properties");
        //获取指定的系统属性
        System.out.println(System.getProperty("os.name"));
    }

}
