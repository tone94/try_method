package cn.pdy.try_method.jdk.java.atemp;

import java.util.Calendar;

/**
 * Java提供了java.util.Date类来处理日期时间, 但是Date类不仅无法实现国际化, 而且对不同属性使用了前后矛盾的偏移量
 * 由于Date的设计缺陷, 所以java不推荐使用Date, 继而提供了Calendar 类来更好的处理时间和日期
 * 但是Calendar又显得过于复杂, 于是Java8吸取了Joda-Time库的经验, 提供了一套全新的日期时间库 java.time
 *
 * 所以此类不进行赘述
 */
public class CalendarTest {

    public static void main(String[] args) {

        //Calendar 是一个抽象类
        //通过静态方法来创建Calendar对象
        //Calendar.getInstance() 通过指定的TimeZone , Locale类来创建, 如果不指定, 则使用默认的
    }
}
