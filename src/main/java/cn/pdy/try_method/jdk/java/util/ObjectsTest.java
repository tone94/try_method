package cn.pdy.try_method.jdk.java.util;

import java.util.Objects;

/**
 * Java7新增, 提供了一些工具方法来操作对象, 这些方法大多是空指针安全的
 */
public class ObjectsTest {

    static Object obj;
    public static void main(String[] args) {

        //obj为空的对象,直接调用方法会报空指针异常
        System.out.println(Objects.toString(obj));
        System.out.println(Objects.hashCode(obj));
        //Objects.requireNonNull(obj,"不能为空");
    }
}
