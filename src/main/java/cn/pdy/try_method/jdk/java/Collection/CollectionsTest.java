package cn.pdy.try_method.jdk.java.Collection;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 操作集合的工具类
 */
public class CollectionsTest {

    public static void main(String[] args) {
        // ----------排序操作-----------
        // reverse(List list) : 反转集合中元素的顺序
        // shuffle(List list) : 对集合元素进行随机排序,洗牌
        // sort(List list) : 自然顺序排序
        // sort(List list,Comparator c) : 根据比较器规则排序
        // swap(List list,int i,int j) : 交换指定index的元素
        // rotate(List list,int distance) : 移动元素

        // -----------查找,替换操作------------
        // int binarySearch(List list,Object key) : 使用二分搜索法搜索指定对象在集合中的索引
        // Object max(List list)
        // Object max(List list,Comparator c)
        // min ..
        // fill(List list,Object o) : 使用指定元素替换集合中的所有元素
        // int frequency(Collection c,Object o) : 返回集合中指定元素的出现次数
        // int indexOfSubList
        // int lastIndexOfSubList
        // bollean replaceAll(List list,Object old,Object new)

        // -----------同步控制-------------
        // 提供了多个 synchronizeXxx() 方法,将指定集合包装成线程同步的集合
        // 解决多线程并发访问集合时的线程安全问题

        // ----------提供了如下方法来返回一个 不可变的集合------------
        // emptyXxx()
        // singletonXxx()
        // unmodifiableXxx()

    }
}
