package cn.pdy.try_method.jdk.java.Collection;

import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * Collection接口是List,Set,Queue接口的父接口
 * 其中定义了一些操作集合元素的方法
 * 介绍了怎么操作集合元素,遍历集合元素
 */
public class CollectionTest implements Collection{

    // 1.使用Lambda表达式遍历集合
    @Test
    public void test1(){
        // 创建一个集合
        Collection books = new HashSet();
        books.add("aaa");
        books.add("bbb");
        books.add("ccc");
        // java8 为Iterable 接口新增了一个 forEach(Consumer action) 默认方法 ,该方法的参数是函数式接口
        // 而Iterable接口是 Collection的父接口, 所以Collection集合也可以直接调用
        // 调用forEach() 方法遍历集合
        books.forEach(obj -> System.out.println("迭代集合元素: " +obj));
    }

    // 2.使用java8增强的Iterator遍历集合元素
    @Test
    public void test2(){
        // 创建一个集合
        Collection books = new HashSet();
        books.add("aaa");
        books.add("bbb");
        books.add("ccc");
        // 获取集合对应的迭代器, Iterator对象必须有一个与之关联的collection对象
        Iterator it = books.iterator();
        while (it.hasNext()){
            String book = (String) it.next();
            System.out.println(book);
            if (book.equals("aaa")){
                // 从集合中删除上一次next方法返回的元素
                // 使用迭代器访问集合元素时,集合里的元素不能被改变,否则会报错.只能通过it的remove来删除集合元素才可以
                it.remove();
            }
            //对book变量赋值,不会改变集合本身元素
            book = "ddd";
        }
        System.out.println(books);
    }

    // 3.使用Lambda表达式遍历Iterator
    @Test
    public void test3(){
        // 创建一个集合
        Collection books = new HashSet();
        books.add("aaa");
        books.add("bbb");
        books.add("ccc");
        // 获取集合对应的迭代器, Iterator对象必须有一个与之关联的collection对象
        Iterator it = books.iterator();
        // 使用Lambda表达式来遍历集合元素
        it.forEachRemaining(obj -> System.out.println("迭代集合元素: " + obj));
    }

    // 4. 使用foreach循环遍历集合元素
    @Test
    public void test4(){
        // 创建一个集合
        Collection books = new HashSet();
        books.add("aaa");
        books.add("bbb");
        books.add("ccc");

        for (Object obj: books) {
            // 此处的集合元素也不能被修改
            System.out.println(obj.toString());
        }
    }

    // 5.使用java8新增的 Predicate 操作集合
    @Test
    public void test5(){
        // 创建一个集合
        Collection books = new HashSet();
        books.add("aaa");
        books.add("bbb");
        books.add("ccc");
        books.add("eeeeee");
        books.add("ffffffffff");
        // 使用Lambda表达式(目标类型是Predicate)过滤集合
        // 删除掉长度小于5的元素
        books.removeIf(ele -> ((String)ele).length() < 5);
        System.out.println(books);
    }

    // 使用Predicate可以充分简化集合的运算, 假设有三个统计需求
    // 1.统计书名中出现"疯狂"字符串的图书数量
    // 2.出现'java'的图书数量
    // 3.书名长度大于10的图书数量
    @Test
    public void test6(){
        // 创建一个集合
        Collection books = new HashSet();
        books.add("疯狂Java讲义");
        books.add("轻量级java ee企业应用实战");
        books.add("疯狂andriod讲义");

        // 若采用传统方式,需要三次循环, 但用Predicate只需要一个方法即可
        // 额.. 我传统方式也可以这么做吧
        System.out.println(calAll(books,ele -> ((String)ele).contains("疯狂")));
    }

    public static int calAll(Collection books, Predicate p){
        int total = 0;
        for(Object obj : books){
            // 使用predicate的 test() 方法判断该对象是否满足指定的条件
            if (p.test(obj)){
                total++;
            }
        }
        return total;
    }

    // 使用java8新增的 Stream 操作集合
    // 看不懂... 略


    // --------------------------------以下是Collection定义的方法--------------------------------------
    public int size() {
        return 0;
    }

    // 判断集合是否为空
    public boolean isEmpty() {
        return false;
    }

    // 返回集合里是否包含某个元素
    public boolean contains(Object o) {
        return false;
    }

    // 返回一个iterator对象, 用于遍历集合里的元素
    public Iterator iterator() {
        return null;
    }

    public Object[] toArray() {
        return new Object[0];
    }

    // 向集合中添加一个元素
    public boolean add(Object o) {
        return false;
    }

    // 删除集合中的指定元素o
    public boolean remove(Object o) {
        return false;
    }

    // 将集合c中的所有元素添加到指定的集合里
    public boolean addAll(Collection c) {
        return false;
    }

    // 清除所有元素,集合长度变为0
    public void clear() {

    }

    // 从集合中删除集合c里不包含的元素
    public boolean retainAll(Collection c) {
        return false;
    }

    // 从集合中删除集合c里包含的所有元素
    public boolean removeAll(Collection c) {
        return false;
    }

    public boolean containsAll(Collection c) {
        return false;
    }

    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
