package cn.pdy.try_method.jdk.java.Collection;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.UnaryOperator;

/**
 * List 集合代表有序,可重复的集合
 * 与Set相比, List增加了根据元素来操作元素的方法, 除此之外, java8还为List添加了两个默认方法
 * 通过equals判断两个对象相等
 */
public class ListTets {

    /**
     * java8 改进的List接口和ListIterator接口
     */
    @Test
    public void test1() {
        // 创建集合
        List list = new ArrayList();
        list.add("疯狂Java讲义");
        list.add("轻量级java ee企业应用实战");
        list.add("疯狂andriod讲义");
        list.add("疯狂IOS讲义");
        // java8 新增默认方法
        // void sort(Comparetor c) : 根据Comparator参数对List集合的元素排序
        list.sort((o1,o2)->((String)o1).length()-((String)o2).length());
        System.out.println(list);

        // void replaceAll(UnaryOperator operator) : 根据operator指定的计算规则重新设置List集合的所有元素
        list.replaceAll(ele->((String)ele).length()); //该表达式控制使用每个字符串的长度作为新的集合元素
        System.out.println(list);
    }

    /**
     * List 提供的 listIterator() 方法,
     * 返回一个 ListIterator对象,继承自Iterator接口 ,增加了向前迭代的方法
     *
     * 注: 向前迭代之前也需要先采用正向迭代
     */
    @Test
    public void test2(){
        // 创建集合
        List list = new ArrayList();
        list.add("疯狂Java讲义");
        list.add("轻量级java ee企业应用实战");
        list.add("疯狂andriod讲义");
        list.add("疯狂IOS讲义");

        ListIterator lit = list.listIterator();
        while (lit.hasNext()){
            System.out.println(lit.next());
            lit.add("---------分隔符---------");
        }
        System.out.println("下面开始反向迭代");
        while(lit.hasPrevious()){
            System.out.println(lit.previous());
        }
    }

    /*
        ArrayList 和 Vector 实现类:
            都是基于数组实现的List类, 所以ArrayList和Vector类封装了一个动态的,允许再分配的Object[]数组
            它们使用 initialCapacity 参数来设置该数组的长度, 当元素超出该数组长度时, 会自动增加. 默认为10
            如果向集合中添加大量元素时, 可使用 ensureCapacity(int minCapacity)方法一次性的增加容量, 可以减少重分配次数,从而提高性能
            trimToSize() 方法调整集合的数组长度为当前元素的个数, 可减少集合对象占用的存储空间
        区别:
            ArrayList 线程不安全, Vector 线程安全.
        但是不推荐使用Vector, Collections工具类可以将ArrayList变成线程安全的

        Vector 还提供了一个子类 Stack ,用于模拟栈. 有peek,pop,push等方法. 仍然不推荐使用,可以考虑用 ArrayDeque
     */

    /*
        Arrays 工具类提供了 asList(Object... a)方法, 返回Arrays的内部类ArrayList的实例
        Arrays.ArrayList 是一个固定长度的List集合,只能遍历,不能增加,删除.
     */

}

