package cn.pdy.try_method.jdk.java.Collection;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 有 HashMap ,LinkedHashMap ,SortedMap ,TreeMap ,EnumMap 等子接口和实现类
 * Map 与 Set 的关系非常密切, Java 先实现了Map, 然后通过包装一个所有 value都为null的Map就实现了Set
 *
 * 用作key的对象必须实现 hashCode()方法和equals()方法
 */
public class MapTest implements Map{

    // -------------Java8 新增的方法--------------------
    // 略..

    /*
        HashMap 线程不安全, Hashtable 线程安全(古老)
        HashMap 允许null, Hashtable 不允许null
     */

    /*
        LinkedHashMap 实现类, 使用双向链表来维护key-value对的次序
     */

    /*
        Properties --> Hashtable的子类 ,可用于读写属性文件
        相当于一个key,value 都是string类型的Map
        方法: setProperty(), load(), store()
     */

    /*
        WeakHashMap ,与HashMap 用法基本相似
        区别:
            HashMap 的key保留了对实际对象的强引用
            Weak.. 保留的是弱引用
     */

    // --------------Map 提供的方法-------------------
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public Object get(Object key) {
        return null;
    }

    @Override
    public Object put(Object key, Object value) {
        return null;
    }

    @Override
    public Object remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map m) {

    }

    @Override
    public void clear() {

    }

    /**
     * 返回Map中所有key组成的set集合
     * @return
     */
    @Override
    public Set keySet() {
        return null;
    }

    /**
     * 返回map中所有value组成的集合
     * @return
     */
    @Override
    public Collection values() {
        return null;
    }

    /**
     * 返回map中包含k-v对所组成的set集合
     * 每个集合元素都是Map.Entry , Entry 是Map的内部类
     */
    @Override
    public Set<Entry> entrySet() {
        return null;
    }
}
