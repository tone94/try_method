package cn.pdy.try_method.jdk.java.time;

import java.time.*;

public class NewDatePackageTest {

    public static void main(String[] args) {

        // --------------关于Clock的用法-----------------
        // 获取当前Clock
        Clock clock = Clock.systemUTC();
        // 通过clock获取当前时刻
        System.out.println(clock.instant());
        // 获取clock对应的毫秒数,与System.currentTimeMillis 输出相同
        System.out.println(clock.millis());
        System.out.println(System.currentTimeMillis());

        // --------------Duration-----------------
        Duration d = Duration.ofSeconds(6000);
        d.toMinutes(); // 分
        d.toHours(); // 时
        d.toDays(); // 天
        //在clock的基础上增加6000秒,返回新的clock
        Clock clock1 = Clock.offset(clock, d);
        System.out.println(clock1.instant());

        // ---------------Instant----------------
        //获取当前时间
        Instant instant1 = Instant.now();
        //添加6000秒,返回新的instant
        Instant instant2 = instant1.plusSeconds(6000);
        //根据字符串解析Instant对象
        Instant instant3 = Instant.parse("2014-02-23T10:12:35.342Z");
        //在instant3的基础上添加5小时4分钟
        Instant instant4 = instant3.plus(Duration.ofHours(5).plusMinutes(4));
        //获取instant4的5天前的时刻
        Instant instant5 = instant4.minus(Duration.ofDays(5));

        // ----------------LocalDate-----------------------
        LocalDate localDate = LocalDate.now();
        //获得2014年的第146天
        localDate = LocalDate.ofYearDay(2014, 146);
        //设置为2014年5月21日
        localDate = LocalDate.of(2014, Month.MAY,21);

        // -----------------LocalTime----------------
        // 获取当前时间
        LocalTime localTime = LocalTime.now();
        // 设置为22点33分
        localTime = LocalTime.of(22,33);
        //返回一天中的第5503秒
        localTime = LocalTime.ofSecondOfDay(5503);

        // ---------------LocalDateTime--------------
        // 获取当前时间日期
        LocalDateTime localDateTime = LocalDateTime.now();
        // 当前时间日期加上25小时3分钟
        LocalDateTime future = localDateTime.plusHours(25).plusMinutes(3);

        // -----------Year, YearMonth, MonthDay--------
        Year year = Year.now(); // 获取当前年份
        year = year.plusYears(5); // 加5年
        // 根据指定月份获取YearMonth
        YearMonth ym = year.atMonth(10);
        // 当前年月再加5年减三个月
        ym = ym.plusYears(5).minusMonths(3);
        MonthDay monthDay = MonthDay.now();
        // 设置为5月23日
        MonthDay md = monthDay.with(Month.MAY).withDayOfMonth(23);
    }
}
