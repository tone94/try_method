package cn.pdy.try_method.jdk.java.lang;

import java.io.IOException;

/**
 * Runtime 类代表java的运行时环境
 * 每个java程序都有个与之对应的Runtime实例, 可以访问JVM的信息, 如处理器数量,内存信息等
 *
 * 提供了 gc(), runFinalization() 方法里通知系统进行垃圾回收
 * load(), loadLibrary() 来加载文件和动态链接库
 */
public class RuntimeTest {
    public static void main(String[] args) throws IOException {
        //获取Runtime对象
        Runtime rt = Runtime.getRuntime();
        //处理器数量
        System.out.println(rt.availableProcessors());
        //空闲内存数
        System.out.println(rt.freeMemory());
        //总内存数
        System.out.println(rt.totalMemory());
        //可用最大内存数
        System.out.println(rt.maxMemory());

        //直接单独启动一个进程来运行操作系统的命令
        rt.exec("notepad.exe");
    }


}
